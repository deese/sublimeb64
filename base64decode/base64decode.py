import sublime, sublime_plugin
import base64

class SublimeBaseDecodeCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        for region in self.view.sel():
            if not region.empty():
                # Get the selected text
                s = self.view.substr(region)
                # Transform it via rot13
                #s = s.encode('rot13')
                try:
                    s = base64.b64decode(s).decode('UTF-8')
                except Exception as e:
                    print("Error: %s" % str(e))
                    pass
                # Replace the selection with transformed text
                self.view.replace(edit, region, s)
